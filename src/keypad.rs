use rppal::gpio::{Gpio, InputPin, OutputPin};
use std::thread::sleep;
use std::time::{Duration, Instant};

pub struct Keypad {
    columns: Vec<OutputPin>,
    rows: Vec<InputPin>,
    keymap: Vec<Vec<char>>,
}

impl Keypad {
    pub fn new_4_by_4() -> Result<Keypad, String> {
        let g = match Gpio::new() {
            Ok(ok) => ok,
            Err(err) => return Err(format!("Error in GPIO access! {}", err)),
        };
        let columns = vec![
            g.get(21).unwrap().into_output(),
            g.get(20).unwrap().into_output(),
            g.get(16).unwrap().into_output(),
            g.get(12).unwrap().into_output(),
        ];
        let rows = vec![
            g.get(26).unwrap().into_input_pulldown(),
            g.get(19).unwrap().into_input_pulldown(),
            g.get(13).unwrap().into_input_pulldown(),
            g.get(6).unwrap().into_input_pulldown(),
        ];
        let keymap = vec![
            vec!['1', '2', '3', 'A'],
            vec!['4', '5', '6', 'B'],
            vec!['7', '8', '9', 'C'],
            vec!['*', '0', '#', 'D'],
        ];
        Ok(Keypad::new(columns, rows, keymap))
    }

    pub fn new(columns: Vec<OutputPin>, rows: Vec<InputPin>, keymap: Vec<Vec<char>>) -> Keypad {
        let mut k = Keypad {
            columns: columns,
            rows: rows,
            keymap: keymap,
        };
        for column in k.columns.iter_mut() {
            column.set_low();
        }
        return k;
    }

    pub fn get_pressed(&mut self) -> Option<char> {
        let states = self.get_states();
        for (i, column) in states.iter().enumerate() {
            for (j, _row) in column.iter().enumerate() {
                if states[i][j] {
                    return Some(self.keymap[i][j]);
                }
            }
        }
        None
    }

    pub fn get_states(&mut self) -> Vec<Vec<bool>> {
        let mut states = vec![vec![false; self.columns.len()]; self.rows.len()];
        for (i, column) in self.columns.iter_mut().enumerate() {
            column.set_high();
            sleep(Duration::from_millis(1));
            for (j, row) in self.rows.iter_mut().enumerate() {
                states[i][j] = row.is_high();
            }
            column.set_low();
        }
        return states;
    }

    pub fn wfi(&mut self, duration: Duration) -> Option<char> {
        let start = Instant::now();
        loop {
            let key = self.get_pressed();
            if key != None {
                loop {
                    let debounce = self.get_pressed();
                    if debounce == None {
                        return key;
                    }
                }
            }
            let now = Instant::now();
            if now - start > duration {
                return None;
            }
        }
    }
}
