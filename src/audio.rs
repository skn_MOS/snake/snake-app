use rodio::{Decoder, Device, Sink};
use std::error::Error;
use std::fs::File;
use std::io::BufReader;

pub struct Audio {
    _device: Device,
    sink: Sink,
    locked: bool,
}

impl Audio {
    pub fn new() -> Option<Audio> {
        let _device = rodio::default_output_device()?;
        let sink = Sink::new(&_device);
        Some(Audio {
            _device,
            sink,
            locked: false,
        })
    }

    pub fn lock(&mut self) {
        self.locked = true;
    }

    pub fn unlock(&mut self) {
        self.locked = false;
    }

    pub fn stop(&mut self) {
        if !self.locked {
            self.sink.stop();
            self.sink = Sink::new(&self._device);
        }
    }

    pub fn play(&mut self, title: &str) -> std::result::Result<(), Box<dyn Error>> {
        if !self.locked {
            self.stop();
            let decoder = self.create_decoder(title)?;
            self.sink.append(decoder);
        }
        Ok(())
    }

    pub fn queue(&mut self, title: &str) -> std::result::Result<(), Box<dyn Error>> {
        if !self.locked {
            let decoder = self.create_decoder(title)?;
            self.sink.append(decoder);
        }
        Ok(())
    }

    pub fn queue_n_times(&mut self, title: &str, n: u8) -> std::result::Result<(), Box<dyn Error>> {
        for _ in 0..n {
            self.queue(title)?;
        }
        Ok(())
    }

    pub fn play_n_times(&mut self, title: &str, n: u8) -> std::result::Result<(), Box<dyn Error>> {
        if !self.locked {
            self.play(title)?;
        }
        self.queue_n_times(title, n - 1)
    }

    fn create_decoder(
        &self,
        title: &str,
    ) -> std::result::Result<Decoder<BufReader<File>>, Box<dyn Error>> {
        let file = File::open(format!("sounds/{}.mp3", title))?;
        let decoder = Decoder::new(BufReader::new(file))?;
        Ok(decoder)
    }
}
