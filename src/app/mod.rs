use chrono::offset::Local;
use chrono::DateTime;
use std::{io, io::Stdout, iter::FromIterator, time::SystemTime};
use termion::{input::MouseTerminal, raw::IntoRawMode, raw::RawTerminal, screen::AlternateScreen};
use tui::{
    backend::TermionBackend,
    layout::{Alignment, Constraint, Corner, Direction, Layout},
    style::{Color, Modifier, Style},
    text::{Span, Spans},
    widgets::{Block, BorderType, Borders, List, ListItem, Paragraph},
    Terminal,
};

mod locker;
use locker::Locker;
use locker::User;
use log;

pub struct App {
    terminal: Terminal<TermionBackend<AlternateScreen<MouseTerminal<RawTerminal<Stdout>>>>>,
    logs: Vec<String>,
    locker: Locker,
    user_to_login: String,
    pin_to_login: Vec<char>,
    incorrect_pin_counter: u8,
    awaits_for_input: bool,
    pub social_left: bool,
    pub social_right: bool,
    pub lab_left: bool,
    pub lab_right: bool,
    pub door: bool,
}

impl App {
    pub fn new() -> App {
        let stdout = io::stdout().into_raw_mode().unwrap();
        let stdout = MouseTerminal::from(stdout);
        let stdout = AlternateScreen::from(stdout);
        let backend = TermionBackend::new(stdout);
        let terminal = Terminal::new(backend).unwrap();

        let mut app = App {
            terminal,
            logs: Vec::new(),
            locker: Locker::default(),
            user_to_login: String::new(),
            pin_to_login: Vec::new(),
            incorrect_pin_counter: 0u8,
            awaits_for_input: false,
            social_left: false,
            social_right: false,
            lab_left: false,
            lab_right: false,
            door: false,
        };

        match Locker::read_db() {
            Ok(ok) => {
                app.locker = ok;
            }
            Err(err) => {
                app.add_log(format!("Error in Locker creation: {}", err.to_string()));
            }
        };

        app
    }

    pub fn draw(&mut self) {
        let mut logs = self.logs.clone();
        logs.reverse();
        let logs: Vec<ListItem> = logs
            .iter()
            .map(|log| ListItem::new(vec![Spans::from(Span::raw(log))]))
            .collect();
        let armed = self.locker.is_armed();
        let last_user = self.locker.get_last_user();
        let last_action_time = self.locker.get_last_action_time();
        let alarmed = self.locker.is_alarmed();

        let lab_left = self.lab_left.clone();
        let lab_right = self.lab_right.clone();
        let social_left = self.social_left.clone();
        let social_right = self.social_right.clone();
        let door = self.door.clone();

        self.terminal
            .draw(|f| {
                let size = f.size();

                let block = Block::default()
                    .title(
                        " S.N.A.K.E. - System Natychmiastowego Alarmowania o Kradzieży Elektroniki",
                    )
                    .borders(Borders::ALL)
                    .border_type(BorderType::Rounded)
                    .style(Style::default().bg(Color::Black));
                f.render_widget(block, size);

                let columns = Layout::default()
                    .direction(Direction::Horizontal)
                    .margin(1)
                    .constraints([Constraint::Percentage(70), Constraint::Percentage(30)].as_ref())
                    .split(size);

                let left_rows = Layout::default()
                    .direction(Direction::Vertical)
                    .margin(1)
                    .constraints([Constraint::Length(8), Constraint::Min(0)].as_ref())
                    .split(columns[0]);

                let right_rows = Layout::default()
                    .direction(Direction::Vertical)
                    .margin(1)
                    .constraints([Constraint::Percentage(70), Constraint::Percentage(30)].as_ref())
                    .split(columns[1]);

                let block = Block::default()
                    .title(get_time())
                    .borders(Borders::ALL)
                    .border_type(BorderType::Rounded);

                let mut logo = Paragraph::new(SNAKE_LOGO).block(block);
                if alarmed {
                    logo = logo.style(
                        Style::default()
                            .fg(Color::Red)
                            .add_modifier(Modifier::RAPID_BLINK),
                    );
                } else if armed {
                    logo = logo.style(Style::default().fg(Color::Red));
                } else {
                    logo = logo.style(Style::default().fg(Color::Green));
                }
                f.render_widget(logo, left_rows[0]);

                let block = Block::default()
                    .title("Dziennik zdarzeń")
                    .borders(Borders::ALL)
                    .border_type(BorderType::Rounded);

                let list = List::new(logs)
                    .block(block)
                    .start_corner(Corner::BottomLeft);
                f.render_widget(list, left_rows[1]);

                let block = Block::default()
                    .title("Stan pomieszczeń")
                    .borders(Borders::ALL)
                    .border_type(BorderType::Rounded);

                let lab_left_span = Span::styled(
                    format!(
                        "\u{25AF}  Okno laboratorium lewe:  {}",
                        if lab_left {
                            "\u{1F512} zamknięte"
                        } else {
                            "\u{1F513} otwarte"
                        }
                    ),
                    Style::default().fg(if lab_left { Color::Green } else { Color::Red }),
                );
                let lab_right_span = Span::styled(
                    format!(
                        "\u{25AF}  Okno laboratorium prawe: {}",
                        if lab_right {
                            "\u{1F512} zamknięte"
                        } else {
                            "\u{1F513} otwarte"
                        }
                    ),
                    Style::default().fg(if lab_right { Color::Green } else { Color::Red }),
                );
                let social_left_span = Span::styled(
                    format!(
                        "\u{25AF}  Okno socjal lewe:        {}",
                        if social_left {
                            "\u{1F512} zamknięte"
                        } else {
                            "\u{1F513} otwarte"
                        }
                    ),
                    Style::default().fg(if social_left {
                        Color::Green
                    } else {
                        Color::Red
                    }),
                );
                let social_right_span = Span::styled(
                    format!(
                        "\u{25AF}  Okno socjal prawe:       {}",
                        if social_right {
                            "\u{1F512} zamknięte"
                        } else {
                            "\u{1F513} otwarte"
                        }
                    ),
                    Style::default().fg(if social_right {
                        Color::Green
                    } else {
                        Color::Red
                    }),
                );
                let door_span = Span::styled(
                    format!(
                        "\u{1F6AA} Drzwi wejściowe:         {}",
                        if door {
                            "\u{1F512} zamknięte"
                        } else {
                            "\u{1F513} otwarte"
                        }
                    ),
                    Style::default().fg(if door { Color::Green } else { Color::Red }),
                );

                let spans = vec![
                    Spans::from(lab_left_span),
                    Spans::from(lab_right_span),
                    Spans::from(social_left_span),
                    Spans::from(social_right_span),
                    Spans::from(door_span),
                ];
                let paragraph = Paragraph::new(spans)
                    .block(block)
                    .alignment(Alignment::Left);
                f.render_widget(paragraph, right_rows[0]);

                let block = Block::default()
                    .title("Stan alarmu")
                    .borders(Borders::ALL)
                    .border_type(BorderType::Rounded);

                let spans = vec![
                    Spans::from(Span::styled(
                        format!(
                            "Stan alarmu:        {}\n",
                            if armed {
                                "\u{1F512} uzbrojony"
                            } else {
                                "\u{1F513} rozbrojony"
                            }
                        ),
                        Style::default().fg(if armed { Color::Red } else { Color::Green }),
                    )),
                    Spans::from(Span::from(format!("Ostatni użytkownik: {}", last_user))),
                    Spans::from(Span::from(format!(
                        "Ostatnia akcja:     {}",
                        last_action_time
                    ))),
                ];
                let paragraph = Paragraph::new(spans)
                    .block(block)
                    .alignment(Alignment::Left);
                f.render_widget(paragraph, right_rows[1]);
            })
            .unwrap();
    }

    pub fn add_log(&mut self, log: String) {
        log::debug!("{}", log);
        let log = format!(
            "{} | {}",
            chrono::Local::now().format("[%Y-%m-%d][%H:%M:%S]"),
            log
        );
        self.logs.push(log);
        if self.logs.len() > 42 {
            self.logs.drain(0..1);
        }
    }

    pub fn get_button(&mut self, key: char) {
        self.pin_to_login.push(key);
    }

    pub fn get_pin_len(&self) -> usize {
        self.pin_to_login.len()
    }

    pub fn get_pin_attempts(&self) -> u8 {
        self.incorrect_pin_counter
    }

    pub fn is_alarm(&self) -> bool {
        self.locker.get_alarm()
    }

    pub fn raise_alarm(&mut self) -> std::io::Result<()> {
        self.add_log(format!("ALARM!!!"));
        self.locker.set_alarm(true)
    }

    pub fn authorize(&mut self) -> Result<(), String> {
        let s = String::from_iter(&self.pin_to_login);
        let result = self.locker.pin_attempt(&self.user_to_login, &s);
        match result {
            Ok(_) => {
                self.locker.set_last_user(self.user_to_login.clone());
                self.locker.set_last_action_time(get_time());
                self.locker.set_alarm(false).unwrap();
                self.incorrect_pin_counter = 0;
            }
            Err(_) => self.incorrect_pin_counter += 1,
        }
        result
    }

    pub fn login_attempt(&mut self, uid: String) -> Result<User, String> {
        let user = self.locker.login_attempt(&uid)?;
        self.user_to_login = user.name.clone();
        self.awaits_for_input = true;
        Ok(user)
    }

    pub fn awaits_for_input(&self) -> bool {
        self.awaits_for_input
    }

    pub fn is_armed(&self) -> bool {
        self.locker.is_armed()
    }

    pub fn clear_pin(&mut self) {
        self.pin_to_login.clear();
    }

    pub fn abort_login(&mut self) {
        self.awaits_for_input = false;
        self.user_to_login.clear();
        self.clear_pin();
    }

    pub fn disarm(&mut self) -> std::io::Result<()> {
        self.locker.set_armed(false)
    }

    pub fn arm(&mut self) -> std::io::Result<()> {
        self.locker.set_armed(true)
    }

    pub fn get_last_user(&self) -> String {
        self.locker.get_last_user()
    }
}

fn get_time() -> String {
    let system_time = SystemTime::now();
    let datetime: DateTime<Local> = system_time.into();
    format!("{}", datetime.format("%d.%m.%Y %T"))
}

pub const SNAKE_LOGO: &str = r" _____    _   _      ___      _   __    _____   
/  ___|  | \ | |    / _ \    | | / /   |  ___|  
\ `--.   |  \| |   / /_\ \   | |/ /    | |__    
 `--. \  | . ` |   |  _  |   |    \    |  __|   
/\__/ /_ | |\  | _ | | | | _ | |\  \ _ | |___ _ 
\____/(_)\_| \_/(_)\_| |_/(_)\_| \_/(_)\____/(_)";
