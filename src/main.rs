mod app;
mod audio;
mod event;
mod keypad;
mod rfid;
mod sensors;
mod timer;

use app::App;
use audio::Audio;
use event::{Event, Events, Key};
use fern;
use std::sync::{
    atomic::{AtomicBool, Ordering},
    Arc,
};
use std::{sync::mpsc, thread, time::Duration};
use timer::Timer;

fn main() {
    fern::Dispatch::new()
        .format(|out, message, _record| {
            out.finish(format_args!(
                "{} | {}",
                chrono::Local::now().format("[%Y-%m-%d][%H:%M:%S]"),
                message
            ))
        })
        .chain(fern::log_file("snake.log").unwrap())
        .apply()
        .unwrap();

    let mut app = App::new();
    let events = Events::new();
    let mut audio = Audio::new().unwrap();

    if app.is_alarm() {
        match app.raise_alarm() {
            Ok(_) => (),
            Err(err) => app.add_log(format!("{}", err)),
        }
        audio.unlock();
        match audio.play_n_times("alert", 42) {
            Ok(()) => (),
            Err(err) => app.add_log(format!("{}", err)),
        }
        audio.lock();
    }

    let event_tx = events.get_tx();
    let (keypad_log_tx, log_rx) = mpsc::channel();

    let keypad_mutex_rx = Arc::new(AtomicBool::new(true));
    let keypad_mutex_tx = keypad_mutex_rx.clone();
    let keypad_jh = thread::spawn(move || {
        let k = keypad::Keypad::new_4_by_4();
        let mut k = match k {
            Ok(ok) => ok,
            Err(err) => {
                keypad_log_tx.send(err).unwrap();
                return;
            }
        };
        while keypad_mutex_rx.load(Ordering::Relaxed) {
            match k.wfi(Duration::from_secs(1)) {
                Some(key) => {
                    event_tx.send(Event::Input(Key::Char(key))).unwrap();
                }
                None => {}
            }
        }
    });

    let mut mrfc = rfid::Rfid::new().unwrap();
    let card_to_be_read_tx = Arc::new(AtomicBool::new(false));
    let card_to_be_read_rx = card_to_be_read_tx.clone();
    mrfc.irq
        .set_async_interrupt(rppal::gpio::Trigger::RisingEdge, move |_x| {
            card_to_be_read_tx.store(true, Ordering::Relaxed);
        })
        .unwrap();
    let mut sensors = sensors::Sensor::new_set().unwrap();
    let mut alarm_timer = Timer::new();
    let mut door_timer = Timer::new();

    loop {
        if let Ok(msg) = log_rx.try_recv() {
            app.add_log(msg);
        }

        if door_timer.is_done() {
            match app.arm() {
                Ok(_) => {
                    app.add_log(String::from("Alarm uzbrojony."));
                    match audio.play("planted") {
                        Ok(_) => (),
                        Err(err) => app.add_log(format!("{}", err)),
                    };
                }
                Err(err) => {
                    app.add_log(err.to_string());
                }
            }
            door_timer.stop();
        }

        if alarm_timer.is_done() {
            match app.raise_alarm() {
                Ok(_) => (),
                Err(err) => app.add_log(format!("{}", err)),
            }
            alarm_timer.stop();
        }

        if card_to_be_read_rx.load(Ordering::Relaxed) {
            match mrfc.read_uid() {
                Ok(uid) => {
                    let user = hex::encode_upper(uid.bytes);
                    app.add_log(format!("Wykryto kartę: \"{}\"", user));
                    if alarm_timer.is_started()
                        || app.is_alarm()
                        || sensors.iter().all(|sensor| {
                            sensor.get_name() == sensors::DOOR_NAME || sensor.is_closed()
                        })
                    {
                        match audio.play("affirmative") {
                            Ok(_) => (),
                            Err(err) => app.add_log(format!("{}", err)),
                        }
                        match app.login_attempt(format!("{}", user)) {
                            Ok(user) => {
                                app.add_log(format!("Użytkownik odnaleziony w bazie: \"{}\". Wprowadź PIN lub anuluj za pomocą [A]", user.name))
                            }
                            Err(err) => app.add_log(format!("{}", err)),
                        };
                    } else {
                        match audio.play("negative") {
                            Ok(_) => (),
                            Err(err) => app.add_log(format!("{}", err)),
                        }
                        app.add_log(format!(
                            "Nie można uzbroić alarmu. Nie wszystkie okna są zamknięte!"
                        ));
                    }
                }
                Err(_) => (),
                // Err(err) => app.add_log(format!("RFID read error: {:?}", err)),
            };
            card_to_be_read_rx.store(false, Ordering::Relaxed);
        }

        for sensor in &mut sensors {
            match sensor.get_name() {
                sensors::LAB_LEFT_NAME => app.lab_left = sensor.is_closed(),
                sensors::LAB_RIGHT_NAME => app.lab_right = sensor.is_closed(),
                sensors::SOCIAL_LEFT_NAME => app.social_left = sensor.is_closed(),
                sensors::SOCIAL_RIGHT_NAME => app.social_right = sensor.is_closed(),
                sensors::DOOR_NAME => app.door = sensor.is_closed(),
                _ => (),
            };

            if sensor.has_changed() {
                if sensor.is_closed() {
                    app.add_log(format!("Zamknięto {}.", sensor.get_name()));
                } else {
                    app.add_log(format!("Otwarto {}.", sensor.get_name()));
                }
                if sensor.get_name() == sensors::DOOR_NAME && !sensor.is_closed() {
                    match audio.play("spotted") {
                        Ok(_) => (),
                        Err(err) => app.add_log(format!("{}", err)),
                    }
                }
            }

            if !alarm_timer.is_started() && !app.is_alarm() && app.is_armed() && !sensor.is_closed()
            {
                app.add_log(format!(
                    "Masz 10 sekund na rozbrojenie alarmu! Przyłóż kartę RFID i wprowadź PIN."
                ));
                match audio.play("timer") {
                    Ok(_) => (),
                    Err(err) => app.add_log(format!("{}", err)),
                }
                match audio.queue_n_times("alert", 42) {
                    Ok(_) => (),
                    Err(err) => app.add_log(format!("{}", err)),
                }
                alarm_timer.for_seconds(10);
                alarm_timer.start();
                audio.lock();
            }
        }

        if let Event::Input(key) = events.next().unwrap() {
            if key == Key::Char('q') {
                keypad_mutex_tx.store(false, Ordering::Relaxed);
                break;
            }

            if app.awaits_for_input() {
                match key {
                    Key::Char(c) => {
                        app.add_log(format!("Wprowadzono: '{}'", c));
                        match audio.play("beep") {
                            Ok(_) => (),
                            Err(err) => app.add_log(format!("{}", err)),
                        }
                        match c {
                            '0'..='9' => app.get_button(c),
                            'A' => {
                                app.abort_login();
                                app.add_log(format!("Logowanie anulowane."));
                            }
                            'D' => {
                                app.clear_pin();
                                app.add_log(format!("Wyczyszczono PIN. Wprowadź go ponownie."));
                            }
                            _ => (),
                        };
                        // if ('0'..'9').contains(&c) {
                        //     app.get_button(c);
                        // } else if c == 'A' {
                        //     app.abort_login();
                        //     app.add_log(format!("Logowanie anulowane."));
                        // } else if c == 'A' {
                        //     app.abort_login();
                        //     app.add_log(format!("Logowanie anulowane."));
                        // }
                    }
                    _ => (),
                }
            }
        }

        if app.awaits_for_input() {
            if app.get_pin_len() == 4 {
                match app.is_armed() {
                    true => match app.authorize() {
                        Ok(_) => {
                            match app.disarm() {
                                Ok(_) => {
                                    app.add_log(format!(
                                        "Alarm rozbrojony. Witaj {}!",
                                        app.get_last_user()
                                    ));
                                    alarm_timer.stop();
                                    audio.unlock();
                                    match audio.play("defused") {
                                        Ok(_) => (),
                                        Err(err) => app.add_log(format!("{}", err)),
                                    }
                                }
                                Err(err) => {
                                    app.add_log(err.to_string());
                                }
                            };
                        }
                        Err(err) => {
                            app.add_log(format!("Błąd rozbrajania: {}", err));
                            match audio.play("negative") {
                                Ok(_) => (),
                                Err(err) => app.add_log(format!("{}", err)),
                            }
                            if app.get_pin_attempts() >= 3 {
                                if !app.is_alarm() {
                                    match app.raise_alarm() {
                                        Ok(_) => (),
                                        Err(err) => app.add_log(format!("{}", err)),
                                    }
                                    audio.unlock();
                                    match audio.play_n_times("alert", 42) {
                                        Ok(_) => (),
                                        Err(err) => app.add_log(format!("{}", err)),
                                    }
                                    audio.lock();
                                }
                            }
                        }
                    },
                    false => match app.authorize() {
                        Ok(_) => {
                            if door_timer.is_started() {
                                app.add_log(format!("Uzbrajanie alarmu anulowane."));
                                door_timer.stop();
                            } else {
                                app.add_log(format!(
                                    "Alarm zostanie uzbrojony za 10 sekund. Bywaj {}!",
                                    app.get_last_user()
                                ));
                                door_timer.for_seconds(10);
                                door_timer.start();
                            }
                        }
                        Err(err) => app.add_log(format!("Arm error: {}", err)),
                    },
                }
                app.abort_login();
            }
        }

        app.draw();
    }

    keypad_jh.join().unwrap();
}
